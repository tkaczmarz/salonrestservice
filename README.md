Powyższa aplikacja umożliwia wykonywanie operacji CRUD na bazie danych z trzema tabelami. 

Aplikacja obsługiwana jest na podstawie czterech metod HTTP (GET, POST, PUT, DELETE) używanych na odpowiednich adresach. Potrzebne dane przesyłane są za pośrednictwem plików JSON. 

W celu uruchomienia trzeba zbudować aplikację wykorzystując narzędzie Maven i powstałe w ten sposób archiwum umieścić na serwerze aplikacji. Do działania potrzebny jest także działający serwer HSQL. Serwis nie posiada interfejsu użytkownika, więc aby wykonywać operacje potrzebny jest requester HTTP. W celach testowych wykorzystany został serwer GlassFish 4.