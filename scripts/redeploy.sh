#!/bin/sh

echo "************ UNDEPLOYING *******************"
asadmin undeploy SalonRest
echo "************ BUILDING **********************"
mvn package
echo "************ DEPLOYING *********************"
asadmin deploy target/SalonRest.war
