package tkaczmarz.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tkaczmarz.model.Address;

@Stateless
public class AddressManager {
	
	@PersistenceContext
	EntityManager em;
	
	public Address getAddress (Long id) {
		return em.find(Address.class, id);
	}
	
	public void addAddress (Address address) {
		em.persist(address);
	}
	
	public void editAddress (Address address) {
		em.merge(address);
	}
	
	public void deleteAddress (Long id) {
		em.remove(em.find(Address.class, id));
	}
}
