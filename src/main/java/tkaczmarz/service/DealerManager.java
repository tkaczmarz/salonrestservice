package tkaczmarz.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tkaczmarz.model.Address;
import tkaczmarz.model.Car;
import tkaczmarz.model.Dealer;

@Stateless
public class DealerManager {

	@PersistenceContext
	EntityManager em;
	
	public Dealer getDealer (Long id) {
		return em.find(Dealer.class, id);
	}
	
	public void addDealer (Dealer dealer) {
		em.persist(dealer);
	}
	
	public void editDealer (Dealer dealer) {
		em.merge(dealer);
	}
	
	public void deleteDealer (Long id) {
		em.remove(em.find(Dealer.class, id));
	}
	
	// Named queries
	
	@SuppressWarnings("unchecked")
	public List<Dealer> getAllDealers () {
		return em.createNamedQuery("dealer.getAll").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Address> getDealerAddress (Long id) {
		return em.createNamedQuery("dealer.getAddress").setParameter("dealerId", id).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Car> getDealerCars (Long id) {
		return em.createNamedQuery("dealer.getAllCars").setParameter("dealerId", id).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Dealer> findDealersByName (String name) {
		String searchPhrase = "%" + name.toLowerCase() + "%";
		return em.createNamedQuery("dealer.findByName").setParameter("searchName", searchPhrase).getResultList();
	}
}
