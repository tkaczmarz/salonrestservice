package tkaczmarz.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tkaczmarz.model.Car;
import tkaczmarz.model.Dealer;

@Stateless
public class CarManager {

	@PersistenceContext(unitName = "demoPU")
	private EntityManager em;
	
	public void addCar (Car car) {
		em.persist(car);
	}
	
	public Car getCar (Long id) {
		return em.find(Car.class, id);
	}
	
	public void editCar (Car car) {
		em.merge(car);
	}
	
	public void deleteCar (Long id) {
		em.remove(em.find(Car.class, id));
	}
	
	// Named queries
	
	@SuppressWarnings("unchecked")
	public List<Car> getAllCars () {		
		return em.createNamedQuery("car.getAll").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Dealer> getDealer (Long id) {		
		return em.createNamedQuery("car.getDealer").setParameter("carId", id).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Car> findCarsByMake (String make) {
		String searchPhrase = "%" + make.toLowerCase() + "%";
		return em.createNamedQuery("car.findByMake").setParameter("makeName", searchPhrase).getResultList();
	}
}
