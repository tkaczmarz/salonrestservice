package tkaczmarz.api;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tkaczmarz.model.Car;
import tkaczmarz.model.Dealer;
import tkaczmarz.service.CarManager;

@Path("cars")
public class CarsResource {

	@EJB
	CarManager cm;
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar (@PathParam("id") Long id) {
		return cm.getCar(id);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Car> getAllCars () {
		return cm.getAllCars();
	}

	@GET
	@Path("/{id}/dealer")
	@Produces(MediaType.APPLICATION_JSON)
	public Dealer getDealer (@PathParam("id") Long id) {
		return cm.getDealer(id).get(0);
	}
	
	@GET
	@Path("/find/{make}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Car> findCarsByMake (@PathParam("make") String make) {
		return cm.findCarsByMake(make);
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response editCar(@PathParam("id") Long id, Car car) {
		car.setId(id);
		cm.editCar(car);
		return Response.ok().build();
	}
}
