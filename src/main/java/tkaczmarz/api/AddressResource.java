package tkaczmarz.api;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import tkaczmarz.model.Address;
import tkaczmarz.service.AddressManager;

@Path("addresses")
public class AddressResource {

	@EJB
	AddressManager am;
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Address getAddress (@PathParam("id") Long id) {
		return am.getAddress(id);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addAddress (Address address) {
		am.addAddress(address);
		return Response.created(null).build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response editAddress (@PathParam("id") Long id, Address address) {
		address.setId(id);
		am.editAddress(address);
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response deleteAddress (@PathParam("id") Long id) {
		am.deleteAddress(id);
		return Response.ok().build();
	}
}
