package tkaczmarz.api;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import tkaczmarz.model.Address;
import tkaczmarz.model.Car;
import tkaczmarz.model.Dealer;
import tkaczmarz.service.CarManager;
import tkaczmarz.service.DealerManager;

@Path("dealers")
public class DealerResource {
	
	@EJB
	DealerManager dm;
	@EJB
	CarManager cm;
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Dealer getDealer (@PathParam("id") Long id) {
		return dm.getDealer(id);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Dealer> getAllDealers () {
		return dm.getAllDealers();
	}
	
	@GET
	@Path("/{id}/address")
	@Produces(MediaType.APPLICATION_JSON)
	public Address getDealerAddress (@PathParam("id") Long id) {
		List<Address> list = dm.getDealerAddress(id);
		return list.get(0);
	}
	
	@GET
	@Path("/{id}/cars")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Car> getDealerCars (@PathParam("id") Long id) {
		return dm.getDealerCars(id);
	}
	
	@GET
	@Path("/find/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Dealer> findDealersByName (@PathParam("name") String name) {
		return dm.findDealersByName(name);
	}
	
	@POST
	@Path("/{id}/cars") 
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addCarToDealer (@PathParam("id") Long id, Car car) {
		
		Dealer dealer = dm.getDealer(id);
		if (dealer == null) {
			return Response.status(Status.NOT_FOUND).build();
		}

		dealer.getCars().add(car);
		dm.editDealer(dealer);
		
		return Response.created(null).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addDealer (Dealer dealer) {
		dm.addDealer(dealer);
		return Response.created(null).build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response editDealer (@PathParam("id") Long id, Dealer dealer) {
		dealer.setId(id);
		dm.editDealer(dealer);
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response deleteDealer (@PathParam("id") Long id) {
		dm.deleteDealer(id);
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/{id}/cars/{carId}")
	public Response deleteCar(@PathParam("id") Long id, @PathParam("carId") Long carId) {
		Dealer dealer = dm.getDealer(id);
		Car car = cm.getCar(carId);
		if (dealer == null || car == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		
		dealer.getCars().remove(car);
		dm.editDealer(dealer);
		
		return Response.ok().build();
	}
}
