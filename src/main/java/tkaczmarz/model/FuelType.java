package tkaczmarz.model;

public enum FuelType {
	Brak, Benzyna, Diesel, Hybryda, LPG, Elektryczny
}
