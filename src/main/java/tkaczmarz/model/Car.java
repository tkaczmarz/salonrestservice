package tkaczmarz.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@NamedQueries ({
		@NamedQuery(name = "car.getAll", query = "SELECT c FROM Car c"),
		@NamedQuery(name = "car.getDealer", query = "SELECT d FROM Dealer d, Car c WHERE c.id = :carId"),
		@NamedQuery(name = "car.findByMake", query = "SELECT c FROM Car c WHERE LOWER(c.make) LIKE :makeName")
})
public class Car {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String make;
	private String model;
	private int yearOfProduction;
	private float engineCapacity;
	private int mileage;
	private int horsePower;
	private FuelType fuelType = null;
	private float cost;
	private boolean used;
	
	public Car () {
		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getMake() {
		return make;
	}
	public void setMake(String brand) {
		this.make = brand;
	}
	
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getYearOfProduction() {
		return yearOfProduction;
	}

	public void setYearOfProduction(int yearOfProduction) {
		this.yearOfProduction = yearOfProduction;
	}

	public float getEngineCapacity() {
		return engineCapacity;
	}

	public void setEngineCapacity(float engineCapacity) {
		this.engineCapacity = engineCapacity;
	}

	public int getMileage() {
		return mileage;
	}
	
	public int getHorsePower() {
		return horsePower;
	}
	
	public void setHorsePower(int horsePower) {
		this.horsePower = horsePower;
	}

	public void setMileage(int mileage) {
		this.mileage = mileage;
	}

	public String getFuelType() {
		return fuelType.toString();
	}

	public void setFuelType(String fuelType) {
		if (!fuelType.isEmpty())
			this.fuelType = FuelType.valueOf(fuelType);
		else
			this.fuelType = FuelType.Brak;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public boolean isUsed() {
		return used;
	}

	public void setUsed(boolean used) {
		this.used = used;
	}
}
